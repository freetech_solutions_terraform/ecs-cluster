# ecs-cluster

Create an AWS ECS cluster. An Amazon EC2 Container Service (Amazon ECS) cluster is a logical grouping of container instances that you can place tasks on.

## Cluster Concepts

 - Clusters can contain multiple different container instance types.
 - Clusters are region-specific.
 - Container instances can only be a part of one cluster at a time.
 - You can create custom IAM policies for your clusters to allow or restrict users' access to specific clusters.

## Dependencies

Dependencies for this module are defined in `Terrafile`.

## Example

Create a `services` cluster

```
module "services_ecs_cluster" {
  source = "../../modules/ecs-cluster"

  vpc_id                      = "module.services_vpc.vpc_id"

  ecs_cluster_name            = "services"

  cloud_init_template_prefix  = "ecs"

  launch_config_key_name      = "var.env["key_name"]"
  launch_config_instance_type = "t2.medium"
  launch_config_image_id      = "module.aws_ecs_ami.ami_id"

  asg_ec2_subnet_ids          = module.services_vpc.private_subnet_ids[0]
  asg_min_size                = "1"
  asg_max_size                = "3"
  asg_desired_capacity        = var.services_ecs_cluster["asg_desired_capacity"]
  asg_tag_names               = var.asg_tag_names

  tags = {
    prefix                    = var.env["prefix"]
    environment               = var.env["name"]
    role                      = "ecs-cluster"
  }
}
```

## Scheduled scale in and out of ASG's instances

To view an example of how to implement scheduling please refer to `tf-aws-ec2-no-elb`'s module documentation.

## Inputs

| Name | Description | Default | Required |
|------|-------------|:-----:|:-----:|
| ecs_cluster_name | The name of the cluster (up to 255 letters, numbers, hyphens, and underscores). This name will also be use to set the tag `ecs_cluster`. | - | yes |
| cloud_init_distro | The OS distribution name | `"amazon_linux"` | no |
| cloud_init_template_prefix | The cloud init template prefix used to build the name of the template | `"common"` | no |
| cloud_init_additional_user_data | Contains additional code (BASH Snippet) to append into the cloud-init template | `""` | no |
| launch_config_image_id | The EC2 image ID to launch | - | yes |
| launch_config_instance_type | The size of instance to launch | `"t2.medium"` | no |
| launch_config_key_name | The key name that should be used for the instance | - | yes |
| launch_config_associate_public_ip_address | Associate a public ip address with an instance in a VPC | `false` | no |
| launch_config_enable_monitoring | Enables/disables detailed monitoring | `false` | no |
| launch_config_root_block_device_volume_type | The type of volume. Can be 'standard', 'gp2', or 'io1' | `"gp2"` | no |
| launch_config_root_block_device_volume_size | The size of the volume in gigabytes | `8` | no |
| launch_config_ebs_optimized | If true, the launched EC2 instance will be EBS-optimized | `false` | no |
| launch_config_ebs_block_devices | Additional EBS block devices to attach to the instance. Refer to documentation on how to use this variable | `[]` | no |
| asg_min_size | The minimum size of the auto scale group | `2` | no |
| asg_max_size | The maximum size of the auto scale group | `2` | no |
| asg_desired_capacity | The number of Amazon EC2 instances that should be running in the group | `2` | no |
| asg_ec2_subnet_ids | A list of subnet IDs to launch resources in | - | yes |
| asg_health_check_grace_period | Time (in seconds) after instance comes into service before checking health | `300` | no |
| asg_health_check_type | Controls how health checking is done. Should be allways 'EC2' as this module does not have ELB | `"EC2"` | no |
| asg_tag_names | A mapping of tag names to assign to the ASG resource. Should match the naming of the keys in tags variable | - | yes |
| asg_schedule_scale_out_min_size | The minimum size for the Auto Scaling group. Defaults to -1 which won't change the minimum size at the scheduled time. | `-1` | no |
| asg_schedule_scale_out_max_size | The maximum size for the Auto Scaling group. Defaults to -1 which won't change the maximum size at the scheduled time. | `-1` | no |
| asg_schedule_scale_out_desired_capacity | The number of EC2 instances that should be running in the group. Defaults to -1 which won't change the desired capacity at the scheduled time. | `-1` | no |
| asg_schedule_scale_out_recurrence | The time when recurring future actions will start. Start time is specified by the user following the Unix cron syntax format. | `""` | no |
| asg_schedule_scale_in_min_size | The minimum size for the Auto Scaling group. Defaults to -1 which won't change the minimum size at the scheduled time. | `-1` | no |
| asg_schedule_scale_in_max_size | The maximum size for the Auto Scaling group. Defaults to -1 which won't change the maximum size at the scheduled time. | `-1` | no |
| asg_schedule_scale_in_desired_capacity | The number of EC2 instances that should be running in the group. Defaults to -1 which won't change the desired capacity at the scheduled time. | `-1` | no |
| asg_schedule_scale_in_recurrence | The time when recurring future actions will start. Start time is specified by the user following the Unix cron syntax format. | `""` | no |
| vpc_id | The ID of the VPC | - | yes |
| tags | A mapping of tags to assign to the resource | - | yes |

## Outputs

| Name | Description |
|------|-------------|
| autoscaling_group_name | The name of the autoscale group |
| ec2_sg_id | The ID of the EC2 Security Group |
| ec2_ssh_sg_id | The ID of the EC2 SSH Security Group |
| iam_role_id | The Amazon Resource Name (ARN) specifying the role |
| ecs_cluster_id | The Amazon Resource Name (ARN) that identifies the cluster |
| ecs_cluster_name | The name of the ECS cluster |

