resource "aws_iam_role_policy_attachment" "ec2_instance" {
  role       = module.ecs_cluster.iam_role_id
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}

