// The name of the autoscale group
output "autoscaling_group_name" {
  value = module.ecs_cluster.autoscaling_group_name
}

// The ID of the EC2 Security Group
output "ec2_sg_id" {
  value = module.ecs_cluster.ec2_sg_id
}

// The ID of the EC2 SSH Security Group
output "ec2_ssh_sg_id" {
  value = module.ecs_cluster.ec2_ssh_sg_id
}

// The Amazon Resource Name (ARN) specifying the role
output "iam_role_id" {
  value = module.ecs_cluster.iam_role_id
}

// The Amazon Resource Name (ARN) that identifies the cluster
output "ecs_cluster_id" {
  value = aws_ecs_cluster.ecs.id
}

// The name of the ECS cluster
output "ecs_cluster_name" {
  value = aws_ecs_cluster.ecs.name
}

