module "ecs_cluster" {
  source = "git@gitlab.com:freetech_solutions_terraform/ec2-no-elb.git"

  vpc_id = var.vpc_id

  cloud_init_distro          = var.cloud_init_distro
  cloud_init_template_prefix = var.cloud_init_template_prefix
  additional_user_data       = var.cloud_init_additional_user_data

  launch_config_image_id                      = var.launch_config_image_id
  launch_config_instance_type                 = var.launch_config_instance_type
  launch_config_key_name                      = var.launch_config_key_name
  launch_config_associate_public_ip_address   = var.launch_config_associate_public_ip_address
  launch_config_enable_monitoring             = var.launch_config_enable_monitoring
  launch_config_root_block_device_volume_type = var.launch_config_root_block_device_volume_type
  launch_config_root_block_device_volume_size = var.launch_config_root_block_device_volume_size

  launch_config_ebs_optimized     = var.launch_config_ebs_optimized
  launch_config_ebs_block_devices = ["${var.launch_config_ebs_block_devices}"]

  asg_ec2_subnet_ids            = var.asg_ec2_subnet_ids
  asg_min_size                  = var.asg_min_size
  asg_max_size                  = var.asg_max_size
  asg_desired_capacity          = var.asg_desired_capacity
  asg_health_check_type         = var.asg_health_check_type
  asg_health_check_grace_period = var.asg_health_check_grace_period

  asg_schedule_scale_out_min_size         = var.asg_schedule_scale_out_min_size
  asg_schedule_scale_out_max_size         = var.asg_schedule_scale_out_max_size
  asg_schedule_scale_out_desired_capacity = var.asg_schedule_scale_out_desired_capacity
  asg_schedule_scale_out_recurrence       = var.asg_schedule_scale_out_recurrence
  asg_schedule_scale_in_min_size          = var.asg_schedule_scale_in_min_size
  asg_schedule_scale_in_max_size          = var.asg_schedule_scale_in_max_size
  asg_schedule_scale_in_desired_capacity  = var.asg_schedule_scale_in_desired_capacity
  asg_schedule_scale_in_recurrence        = var.asg_schedule_scale_in_recurrence

  asg_tag_names = var.asg_tag_names

  tags = merge(
    var.tags,
    {
      "ecs_cluster" = var.ecs_cluster_name
    },
  )
}

