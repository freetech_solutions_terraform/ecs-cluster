## 1.0.0 (October 2, 2019)

BACKWARDS INCOMPATIBILITIES / NOTES:

 * This repo is backwards incompatible because there is a lot of changes in the syntax. When you use v0.11 is not possible to apply changes in code written in v0.12 or above.

FEATURES:

 * Upgraded to v0.12.9

## 0.4.0 (February 6, 2019)

Bugfix:

* Fix Tags merging order, so the Name tag gets set correctly.

## 0.3.0 (November 23, 2018)

IMPROVEMENTS:

 * Removed all Puppet related variables and features.

## 0.2.0 (September 11, 2018)

FEATURES:

 * Added `cloud_init_additional_user_data` variable to support `tf-aws-ec2-no-elb`'s module `additional_user_data` functionality.

## 0.1.0 (September 10, 2018)

FEATURES:

 * Add support for scheduling ASG instances.

## 0.0.2 (July 30, 2018)

IMPROVEMENTS:

 * Add support for additional EBS block devices to attach to the instance.

## 0.0.1 (June 5, 2017)

Initial Release
